package ictgradschool.industry.lab14.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * 
 * The application allows the user to enter a value for N, and then calculates 
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 *
 */
public class IntermediatePrimeFactorsSwingApp extends JPanel {

	private JButton _startBtn;        // Button to start the calculation process.
	private JTextArea _factorValues;  // Component to display the result.
	private JButton _abortBtn;

	PrimeFactorisationWorker pfWorker;

	private class PrimeFactorisationWorker extends SwingWorker<Void, Long> {

		private Long _number;
		private int processCounter = 0;

		public PrimeFactorisationWorker(Long _number) {
			this._number = _number;
		}

		@Override
		protected Void doInBackground() throws Exception {

			long tempN = _number;

			for (long factor = 2; factor*factor <= tempN; factor++) {

				if(isCancelled()) {
					return null;
				}
				// if factor is a factor of n, repeatedly divide it out
				while(tempN % factor == 0) {
//					primeFactors.add(factor);
					publish(factor);
					tempN = tempN / factor;
				}
			}
			// if biggest factor occurs only once, n > 1
			if (tempN > 1) {
//				primeFactors.add(tempN);
				publish(tempN);
			}
			return null;
		}

		@Override
		protected void done() {
//			List<Long> primeFactors = null;
//			try {
//				primeFactors = get();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			} catch (ExecutionException e) {
//				e.printStackTrace();
//			}
//			// below exception is not in the slides?
//			catch (CancellationException e) {
//				_factorValues.setText("Computation aborted!");
//				_startBtn.setEnabled(true);
//				_abortBtn.setEnabled(false);
//				return;
//			}

//			_factorValues.setText("Prime Factors of " + _number + "\n" + stringifyList(primeFactors));

			// Re-enable the Start button.
			_startBtn.setEnabled(true);

			_abortBtn.setEnabled(false);

			// Restore the cursor.
			setCursor(Cursor.getDefaultCursor());

			processCounter = 0;
		}

		@Override
		protected void process(List<Long> results) {
			processCounter++;

			// why is there only one publish call printed to console while there are several for the ProgressBarDemo?
			// does looping through results re-publish previous results?
			System.out.println("Publish call number: " + processCounter);

			for (Long l: results) {
				_factorValues.append(l + "\n");
			}
		}
	}

	public IntermediatePrimeFactorsSwingApp() {
		// Create the GUI components.
		JLabel lblN = new JLabel("Value N:");
		final JTextField tfN = new JTextField(20);

		_startBtn = new JButton("Compute");
		_abortBtn = new JButton("Abort");
		_abortBtn.setEnabled(false);
		_factorValues = new JTextArea();
		_factorValues.setEditable(false);

		// Add an ActionListener to the start button. When clicked, the
		// button's handler extracts the value for N entered by the user from
		// the textfield and find N's prime factors.
		_startBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {

				String strN = tfN.getText().trim();
				long n = 0;

				try {
					n = Long.parseLong(strN);
				} catch(NumberFormatException e) {
					System.out.println(e);
				}

				pfWorker = new PrimeFactorisationWorker(n);
				pfWorker.execute();

				// Disable the Start button until the result of the calculation is known.
				_startBtn.setEnabled(false);

				// Clear any text (prime factors) from the results area.
				_factorValues.setText("");
				_abortBtn.setEnabled(true);

				// Set the cursor to busy.
//				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

//				// Start the computation in the Event Dispatch thread.
//				 for (long i = 2; i*i <= n; i++) {
//
//			            // If i is a factor of N, repeatedly divide it out
//			            while (n % i == 0) {
//			                _factorValues.append(i + "\n");
//			                n = n / i;
//			            }
//			     }
//
//			     // if biggest factor occurs only once, n > 1
//			     if (n > 1) {
//			    	 _factorValues.append(n + "\n");
//			     }
//
//			     // Re-enable the Start button.
//				_startBtn.setEnabled(true);
//
//				// Restore the cursor.
//				setCursor(Cursor.getDefaultCursor());
			}
		});

		_abortBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// what is the difference between true and false if passed as a parameter to cancel()?
				pfWorker.cancel(true);
			}
		});

		// Construct the GUI.
		JPanel controlPanel = new JPanel();
		controlPanel.add(lblN);
		controlPanel.add(tfN);
		controlPanel.add(_startBtn);
		controlPanel.add(_abortBtn);

		JScrollPane scrollPaneForOutput = new JScrollPane();
		scrollPaneForOutput.setViewportView(_factorValues);

		setLayout(new BorderLayout());
		add(controlPanel, BorderLayout.NORTH);
		add(scrollPaneForOutput, BorderLayout.CENTER);
		setPreferredSize(new Dimension(500,300));
	}

	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Prime Factorisation of N");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new IntermediatePrimeFactorsSwingApp();
		frame.add(newContentPane);

		// Display the window.
		frame.pack();
        frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private String stringifyList(List<Long> myList) {
		String list = "";
		for (Long l: myList) {
			list += l + "\n";
		}
		return list;
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

